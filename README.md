#### DISCLAIMER: this app is not secured and is only intended for local use.

# mock-i18n-management

## Introduction

mock-i18n-management is a tool designed to streamline the management of internationalization (i18n) mock files 
in your web applications. If you are tired of manually copying and pasting content across multiple language 
files or constantly performing search-and-replace for different locales (-en, -fr, etc.), this tool is for you.

## Installation

Clone the repository:

```
git clone https://gitlab.com/devs-tools/mock-i18n-management.git
```

Install dependencies:
```
npm install
```
Run the App
```
npm start
```
Open your browser and go to http://localhost:3000.

## Features:

- Create/Update i18n labels based on translation key
  - Based on a key, you can create or update labels in all translation files
![img.png](readme-assets/img.png)
- Search for label values
  - Search for text inside all labels of the defined master file
![img_1.png](readme-assets/img_1.png)
- Search for key values
  - Search for text inside all keys of the defined master file
![img_2.png](readme-assets/img_2.png)
- Compare files
  - Compare all translation files with the defined master file, and get a full diff view.
  - (feature) automatically fix diff files by replacing with the master file content.
![img_3.png](readme-assets/img_3.png)
- Settings
  - Select master file
  - Translation label suffix as uppercase | lowercase
![img_4.png](readme-assets/img_4.png)
  - Change i18n files location → Choose where to look for i18n mock files
![img_5.png](readme-assets/img_5.png)

## Results: 
![img_6.png](readme-assets/img_6.png)

## How to:

### Add new i18n folder

In the "Manage i18n folder paths" section you have the possibility to change the i18n folders location to match your local configuration. Please note that the path to insert is relative to the location of this project

### Add new language file

- In your i18n folder, create a new .json file with the language letter code you want to setup after each label (e.a: -EN: en.json).
- Open that file to insert:
  - Or an empty json object : {} (→ this is the mandatory starting point for the app to be able to read your file)
  - Or a copy of an existing language.json file you already have (→ please note that you'll have to update the existing labels suffix manually)
- Note that in both cases the already existing key/value will not be updated. The app is currently only handling create/update of labels.
  - → feature coming soon !
