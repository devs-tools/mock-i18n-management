# i18n - dev management

- end of fix case
- ~~deactivate -en suffix → option (DONE)~~
- auto fix suffix in master language
- propagate new regexp for suffix (warning-service - :91)
- ~~confirm action dialog (simple html dialog)~~

- ~~prompt for path(s) (DONE)~~
- ~~small nav: (DONE)~~
  - ~~edit (DONE)~~
  - ~~view json (DONE)~~
  - ~~enter path (DONE)~~
- ~~.js in separated folder (DONE)~~
- ~~write new paths (DONE)~~
- ~~writing in files:~~
  - ~~enforce 4 space tabs (DONE)~~

- BUG
  - ~~already exists in each file warning not accurate !!!!~~ 


- functional
  - ~~sanitize " chars !!!!!! → html escaping is enough ?~~
  - ~~html sanitizing in json rendering (keys search) (DONE)~~
  - available languages (add/remove → settings ?) → currently based on file names 'en.json → -EN'
  - manage folder:
    - ~~edit/autofill with current path~~
    - add/remove on inputs instead of global
      - → edit all / edit btn on each field
      - ~~add 'relativeTo' info~~
  - search for values
    - exclusive/inclusive (exact match or inside string) → currently inclusive, checking on 'key.includes'
    - Allow search/replace of keys

  - ~~fix write value before asking for confirm overwrite in other files~~
  - ~~compare files !!~~
  - fix .innerHtml. use css classes to show/hide (what for repeat of containers ?)
    - → Base sanitizing (escaping) of '<' works fine.
    - → set as optional ??

- ~~footer with results number ?~~

- search feature:
  - ~~fix display (DONE)~~
  - button aside the key to edit a translation value
  - ~~add search for key (DONE)~~
    - ~~search form key.sub-key.sub....~~

  - ~~test speed on huge files (OK)~~

- config
  - ~~change /config/file-paths.json name (DONE)~~
  - ~~move app.js to /scripts + rename to index.js (done)~~
  - ~~includes in index.js instead of index.ejs (not possible)~~
  - ~~name style.css + split css in files + includes in index.css~~


- settings menu
  - ~~master file for search /-- file comparator~~
  - ~~folders paths~~
  - languages files / auto creation ?

~~continue global settings display update on select change~~
 - ~~update settings func with new params~~