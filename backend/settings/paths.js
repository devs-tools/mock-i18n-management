const fs = require('fs');
const path = require('path');

const configPath = path.join(__dirname, '../../config/config.json');

module.exports = {
    getFilePaths: () => {
        try {
            const configData = fs.readFileSync(configPath);
            const config = JSON.parse(configData);
            return config.i18nFoldersPaths || [];
        } catch (error) {
            console.error('Error reading config file:', error);
            return [];
        }
    },
    setFilePaths: (newPaths) => {
        const configData = fs.readFileSync(configPath);
        const config = JSON.parse(configData);

        config.i18nFoldersPaths = newPaths;

        fs.writeFileSync(configPath, JSON.stringify(config, null, 4));

        return {
            message: 'File paths updated successfully!',
            newPaths
        };
    }
};