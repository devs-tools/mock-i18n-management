const fs = require('fs');
const path = require('path');

const configPath = path.join(__dirname, '../../config/config.json');

module.exports = {
    getGlobalSettings: () => {
        try {
            const configData = fs.readFileSync(configPath);
            const config = JSON.parse(configData);
            return config.settings || {};
        } catch (error) {
            console.error('Error reading config file:', error);
            return {};
        }
    },
    setGlobalSettings: (settings) => {
        const configData = fs.readFileSync(configPath);
        const config = JSON.parse(configData);
        config.settings = settings;

        fs.writeFileSync(configPath, JSON.stringify(config, null, 4));

        return {
            message: 'Settings updated successfully!',
            settings
        };
    }
};