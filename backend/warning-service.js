const fs = require("fs");
const path = require("path");
const paths = require("./settings/paths");
const {getGlobalSettings} = require("./settings/global-settings");

let i18nFoldersPaths = paths.getFilePaths();
let globalSettings = getGlobalSettings();

module.exports = {
    getGlobalWarnings: () => {
        updateVars();
        const caseWarnings = checkUpperCase();
        const suffixMasterFileWarnings = checkSuffixMasterFile();
        const folderPathsWarnings = checkFolderPaths();
        let warnings = {
            globalSettings,
            i18nFoldersPaths,
            warnings: {
                caseWarnings,
                suffixMasterFileWarnings,
                folderPathsWarnings
            }
        };

        return warnings;
    },
    fixCaseWarning: () => {
        updateVars();
        return fixCaseWarningOperator();
    },
    fixSuffixInMasterLanguageWarning: () => {
        updateVars();
        return fixSuffixInMasterLanguageWarningOperator();
    }
}

function checkUpperCase() {
    const caseDiscrepancies = [];
    const uppercaseLanguageFlag = globalSettings.uppercaseLanguageFlag;

    const checkNestedValues = (obj) => {
        let discrepancyCount = 0;

        const checkObject = (data) => {
            for (const key in data) {
                const value = data[key];
                if (typeof value === 'string') {
                    if (uppercaseLanguageFlag && value.match(/-[a-z]{2,3}$/)) {
                        discrepancyCount++;
                    } else if (!uppercaseLanguageFlag && value.match(/-[A-Z]{2,3}$/)) {
                        discrepancyCount++;
                    }
                } else if (typeof value === 'object' && value !== null) {
                    checkObject(value);
                }
            }
        };

        checkObject(obj);
        return discrepancyCount;
    };

    i18nFoldersPaths.forEach(i18nFolderPath => {
        const files = fs.readdirSync(i18nFolderPath);

        files.forEach(file => {
            const filePath = path.join(i18nFolderPath, file);
            const data = fs.readFileSync(filePath, 'utf8');
            let jsonData;

            try {
                jsonData = JSON.parse(data);
            } catch (parseError) {
                console.error('Error parsing JSON in file:', filePath);
                return;
            }
            const caseDiscrepanciesCount = checkNestedValues(jsonData);

            if (caseDiscrepanciesCount) {
                caseDiscrepancies.push({filepath: filePath, discrepanciesCount: caseDiscrepanciesCount});
            }
        });
    });
    return caseDiscrepancies;

}

function checkSuffixMasterFile() {
    const suffixInMasterLanguageDiscrepancies = [];
    const suffixInMasterLanguage = globalSettings.suffixInMasterLanguage;

    const checkNestedValues = (obj, suffix) => {
        let missingSuffixCount = 0;
        let extraSuffixCount = 0;
        const suffixMatcher = `-${suffix}$`
        const suffixRegexp = new RegExp(suffixMatcher, 'i') // case insensitive

        const checkObject = (data) => {
            for (const key in data) {
                const value = data[key];
                if (typeof value === 'string') {
                    if (!suffixInMasterLanguage && value.match(suffixRegexp)) {
                        extraSuffixCount++;
                    } else if (suffixInMasterLanguage && !value.match(suffixRegexp)) {
                        missingSuffixCount++;
                    }
                } else if (typeof value === 'object' && value !== null) {
                    checkObject(value);
                }
            }
        };

        checkObject(obj);
        return { missingSuffixCount, extraSuffixCount };
    };

    i18nFoldersPaths.forEach(i18nFolderPath => {
        const files = fs.readdirSync(i18nFolderPath);

        files.forEach(file => {
            if (file === globalSettings.masterTranslationFile.filename) {
                const filePath = path.join(i18nFolderPath, file);
                const data = fs.readFileSync(filePath, 'utf8');
                let jsonData;

                try {
                    jsonData = JSON.parse(data);
                } catch (parseError) {
                    console.error('Error parsing JSON in file:', filePath);
                    return;
                }
                const rawSuffix = file.replace('.json', '');
                const suffix = globalSettings.uppercaseLanguageFlag ? rawSuffix.toUpperCase() : rawSuffix.toLowerCase();
                const suffixInMasterLanguageDiscrepanciesCount = checkNestedValues(jsonData, suffix);

                if (suffixInMasterLanguageDiscrepanciesCount.extraSuffixCount || suffixInMasterLanguageDiscrepanciesCount.missingSuffixCount) {
                    suffixInMasterLanguageDiscrepancies.push({filepath: filePath, discrepanciesCount: suffixInMasterLanguageDiscrepanciesCount});
                }
            }
        });
    });
    return suffixInMasterLanguageDiscrepancies;
}

function checkFolderPaths() {
    return !i18nFoldersPaths.length > 0;
}


function fixCaseWarningOperator() {
    const isUppercase = globalSettings.uppercaseLanguageFlag;
    const modifications = [];

    i18nFoldersPaths.forEach(i18nFolderPath => {
        const files = fs.readdirSync(i18nFolderPath);

        files.forEach(file => {
            const filePath = path.join(i18nFolderPath, file);
            const data = fs.readFileSync(filePath, 'utf8');
            const rawSuffix = file.replace('.json', '');
            const suffix = globalSettings.uppercaseLanguageFlag ? rawSuffix.toUpperCase() : rawSuffix.toLowerCase();
            const suffixMatcher = `-${suffix}$`


            let jsonData;

            try {
                jsonData = JSON.parse(data);
            } catch (parseError) {
                console.error('Error parsing JSON in file:', filePath);
                return;
            }

            let changes = [];

            const updateNestedValues = (obj) => {
                for (const key in obj) {
                    const value = obj[key];

                    if (typeof value === 'string'){
                        let updatedValue;

                        /* todo check & change suffix length (here 2 or 3)
                            || use file.replace('.json', '') as matcher
                            → expect each file to be clean (!current jp.json → -EN)
                            */

                        const lowercaseRegex = new RegExp(suffixMatcher.toLowerCase());
                        const uppercaseRegex = new RegExp(suffixMatcher.toUpperCase());
                        if (isUppercase && value.match(/-[a-z]{2,3}$/)) {
                            // suffix lowercase → to UPPERCASE
                            const currentSuffix = value.match(/-[a-z]{2,3}$/)[0];
                            const updatedSuffix = '-' + file.replace('.json', '').toUpperCase()
                            updatedValue = value.replace(currentSuffix, updatedSuffix);
                        } else if (!isUppercase && value.match(/-[A-Z]{2,3}$/)) {
                            // suffix uppercase → to LOWERCASE
                            const currentSuffix = value.match(/-[A-Z]{2,3}$/)[0];
                            const updatedSuffix = '-' + file.replace('.json', '').toLowerCase()
                            updatedValue = value.replace(currentSuffix, updatedSuffix);
                        }

                        if (updatedValue) {
                            const change = {
                                id: changes.length,
                                prev: obj[key],
                                curr: null,
                            };
                            obj[key] = updatedValue;
                            change.curr = obj[key];
                            changes.push(change);
                        }
                    } else if (typeof value === 'object' && value !== null) {
                        updateNestedValues(value);
                    }
                }
            };

            updateNestedValues(jsonData);

            // if (changes.length) {
            //     fs.writeFileSync(filePath, JSON.stringify(jsonData, null, 4), 'utf8');
            //     console.log('Updated language suffixes in:', filePath);
            // } // todo fix case
            if (changes.length) {
                console.log('\nfilepath: ', filePath);
                console.log('changes: ', changes);
                modifications.push({ filepath: filePath, changes });
            }
        });
    });

    return modifications;
}

function fixSuffixInMasterLanguageWarningOperator() {
    // todo fix suffix
    const caseDiscrepancies = [];
    const uppercaseLanguageFlag = globalSettings.uppercaseLanguageFlag;
    const masterLanguageFile = globalSettings.masterTranslationFile.filename;
    const suffixInMasterLanguage = globalSettings.suffixInMasterLanguage;

    const checkNestedValues = (obj) => {
        let discrepancyCount = 0;

        // const checkObject = (data) => {
        //     for (const key in data) {
        //         const value = data[key];
        //         if (typeof value === 'string') {
        //             if (suffixInMasterLanguage && !value.match(/-[a-z]{2,3}$/i)) {
        //                 discrepancyCount++;
        //             } else if (!suffixInMasterLanguage && value.match(/-[A-Z]{2,3}$/)) {
        //                 discrepancyCount++;
        //             }
        //         } else if (typeof value === 'object' && value !== null) {
        //             checkObject(value);
        //         }
        //     }
        // };

        checkObject(obj);
        return discrepancyCount;
    };

    i18nFoldersPaths.forEach(i18nFolderPath => {
        const files = fs.readdirSync(i18nFolderPath);

        files.forEach(file => {
            if (file === masterLanguageFile) {
                const filePath = path.join(i18nFolderPath, file);
                const data = fs.readFileSync(filePath, 'utf8');
                let jsonData;

                try {
                    jsonData = JSON.parse(data);
                } catch (parseError) {
                    console.error('Error parsing JSON in file:', filePath);
                    return;
                }
                const caseDiscrepanciesCount = checkNestedValues(jsonData);

                if (caseDiscrepanciesCount) {
                    caseDiscrepancies.push({filepath: filePath, discrepanciesCount: caseDiscrepanciesCount});
                }
            }
        });
    });
    return caseDiscrepancies;

    // return 'fixSuffixInMasterLanguageWarningOperator() NOT IMPLEMENTED';
}


function updateVars() {
    i18nFoldersPaths = paths.getFilePaths();
    globalSettings = getGlobalSettings();
}