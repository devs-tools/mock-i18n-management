const express = require('express');
const router = express.Router();
const paths = require('./settings/paths');
const globalSettings = require('../backend/settings/global-settings');
const { updateFiles } = require("../backend/translation-files.service");
const {searchTranslationValue, searchTranslationKey, compareFiles} = require("./translation-files.service");
const fs = require("fs");
const {getGlobalWarnings, fixCaseWarning, fixSuffixInMasterLanguageWarning} = require("./warning-service");

// TRANSLATIONS
router.post('/update', (req, res) => {
    const key = req.body.key;
    const value = req.body.value;
    const confirmation = req.body.confirmation;
    const response = updateFiles(key, value, confirmation);
    res.json(response);
});

router.get('/search', (req, res) => {
    const searchTerm = req.query.term;

    if (!searchTerm) {
        return res.status(400).json({ error: 'Search term is required.' });
    }

    const searchResults = searchTranslationValue(searchTerm);
    res.json(searchResults);
});
router.get('/search-key', (req, res) => {
    const searchTerm = req.query.term;

    if (!searchTerm) {
        return res.status(400).json({ error: 'Search term is required.' });
    }

    const searchResults = searchTranslationKey(searchTerm);
    res.json(searchResults);
});
// END TRANSLATIONS


// FILE COMPARATOR
router.get('/compare', (req, res) => {
    res.json(compareFiles());
});
// END FILE COMPARATOR


//////////////
/* SETTINGS */
//////////////

// GLOBAL SETTINGS
router.get('/getGlobalSettings', (req, res) => {
    const settings = globalSettings.getGlobalSettings();

    res.json(settings);
});

router.post('/updateGlobalSettings', (req, res) => {
    const settings = req.body;

    res.json(globalSettings.setGlobalSettings(settings));
});
// END GLOBAL SETTINGS


// PATHS
router.get('/getFilePaths', (req, res) => {
    const i18nFoldersPaths = paths.getFilePaths();
    const relativeTo = __dirname.replaceAll('\\', '/').replace('/backend', '');

    res.json({ i18nFoldersPaths, relativeTo });
});

router.get('/getFilePathsSettings', (req, res) => {
    const i18nFoldersPaths = paths.getFilePaths();
    let response = []
    for (const i18nFoldersPath of i18nFoldersPaths) {
        const files = fs.readdirSync(i18nFoldersPath);
        response.push({
            folderPath: i18nFoldersPath,
            files
        })
    }

    res.json({ response });
});

router.post('/updatePaths', (req, res) => {
    res.json(paths.setFilePaths(req.body.folderPaths));
});
// END PATHS

//////////////////
/* END SETTINGS */
//////////////////


// GLOBAL WARNINGS
router.get('/getWarnings', (req, res) => {
    const globalWarningData = getGlobalWarnings();

    res.json({ ...globalWarningData });
});
// END GLOBAL WARNINGS
// FIXES
router.get('/fixCase', (req, res) => {
    const fixCaseResults = fixCaseWarning();

    res.json({ fixCaseResults });
});
router.get('/fixSuffix', (req, res) => {
    const fixSuffixResults = fixSuffixInMasterLanguageWarning();

    res.json({ fixSuffixResults });
});
// END FIXES


module.exports = router;
