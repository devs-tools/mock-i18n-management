module.exports = {
    setNestedKey: (obj, key, value, languageCode) => {
        const keys = key.split('.');
        const lastKey = keys.pop();
        keys.reduce((nestedObj, nestedKey) => {
            if (!nestedObj[nestedKey]) {
                nestedObj[nestedKey] = {};
            }
            return nestedObj[nestedKey];
        }, obj)[lastKey] = languageCode ? `${value}-${languageCode}` : `${value}`;
    },

    getNestedValue: (obj, key) => {
        const keys = key.split('.');
        let nestedValue = obj;

        for (const k of keys) {
            if (nestedValue && nestedValue[k]) {
                nestedValue = nestedValue[k];
            } else {
                return null;
            }
        }

        return nestedValue;
    },

    keyExists: (obj, key) => {
        const keys = key.split('.');
        let nestedObj = obj;
        for (const k of keys) {
            if (nestedObj && nestedObj[k]) {
                nestedObj = nestedObj[k];
            } else {
                return false;
            }
        }
        return true;
    },

    searchTranslation: (translations, searchTerm) => {
        let results = [];

        const searchRecursive = (obj, path) => {
            for (const key in obj) {
                let fullPath = '' + path;
                fullPath = path ? `${path}.${key}` : key;

                if (typeof obj[key] === 'object' && !fullPath.includes(searchTerm)) {
                    searchRecursive(obj[key], fullPath);
                } else {
                    if (fullPath.includes(searchTerm)) {
                        results.push({
                            id: results.length + 1,
                            path: fullPath,
                            key: key,
                            value: obj[key]
                        })
                    }
                }
            }
        };

        searchRecursive(translations, '');

        return results;
    },
}