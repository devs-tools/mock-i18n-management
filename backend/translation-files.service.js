const fs = require("fs");
const path = require("path");
const paths = require("./settings/paths");
const {
    setNestedKey,
    getNestedValue,
    keyExists,
    searchTranslation
} = require("./translation-key-management");
const {getFilePaths} = require("./settings/paths");
const {getGlobalSettings} = require("./settings/global-settings");

let i18nFoldersPaths = paths.getFilePaths();
let globalSettings = getGlobalSettings();

module.exports = {
    updateFiles: (key, value, confirmation) => {
        i18nFoldersPaths = paths.getFilePaths();
        globalSettings = getGlobalSettings();
        let warning = null;
        let existingData = [];
        let existsInAllFiles = true;
        let existsInSomeFiles = false;
        let filesWhereNotPresent = [];

        // Check if the key exists in all files and gather existing data
        i18nFoldersPaths.forEach(i18nFolderPath => {
            const files = fs.readdirSync(i18nFolderPath);

            files.forEach(file => {
                const filePath = path.join(i18nFolderPath, file);
                const data = fs.readFileSync(filePath, 'utf8');
                let jsonData;

                try {
                    jsonData = JSON.parse(data);
                } catch (parseError) {
                    console.error('Error parsing JSON in file:', filePath);
                    return;
                }

                if (keyExists(jsonData, key)) {
                    const existingValue = getNestedValue(jsonData, key);
                    existingData.push({file: i18nFolderPath + '/' + file, existingValue});
                    existsInSomeFiles = true;
                } else {
                    existsInAllFiles = false;
                    filesWhereNotPresent.push(i18nFolderPath + '/' + file);
                }
            });
        });

        // Set warning based on the key's existence in all or some files
        if (existsInAllFiles && !confirmation) {
            warning = `The key <pre>"${key}"</pre> already exists in all files (see values below). Do you want to overwrite ?`;
        } else if (existsInSomeFiles && !confirmation) {
            warning = `The key <pre>"${key}"</pre> already exists in some files (see values below). Do you want to continue ?`;
        }

        // Write the value if the key doesn't exist in any file or confirmation is true
        if ((!existsInAllFiles && !existsInSomeFiles) || confirmation) {
            i18nFoldersPaths.forEach(i18nFolderPath => {
                const files = fs.readdirSync(i18nFolderPath);

                files.forEach(file => {
                    const filePath = path.join(i18nFolderPath, file);
                    const data = fs.readFileSync(filePath, 'utf8');
                    let jsonData;

                    try {
                        jsonData = JSON.parse(data);
                    } catch (parseError) {
                        console.error('Error parsing JSON in file:', filePath);
                        return;
                    }


                    let languageCode;

                    // suffix in master language setting
                    if (!globalSettings.suffixInMasterLanguage && file === globalSettings.masterTranslationFile.filename) {
                        console.log('empty suffix for master translation file', globalSettings.masterTranslationFile.filename, file)
                        languageCode = '';
                    } else {
                        console.log('else, adding suffix for master file')
                        // uppercase setting
                        if (globalSettings.uppercaseLanguageFlag) {
                            languageCode = file.split('.')[0].toUpperCase();
                        } else {
                            languageCode = file.split('.')[0];
                        }
                    }
                    setNestedKey(jsonData, key, value, languageCode);
                    const updatedData = JSON.stringify(jsonData, null, 4);
                    fs.writeFileSync(filePath, updatedData, 'utf8');
                    console.log('Updated keys in:', filePath);
                });
            });
        }

        return warning ? {warning, existingData, filesWhereNotPresent} : {};
    },

    searchTranslationValue: (searchValue) => {
        i18nFoldersPaths = paths.getFilePaths();
        globalSettings = getGlobalSettings();
        const filePath = path.join(
            getFilePaths()[globalSettings.masterTranslationFile.i18nFolderIndex],
            globalSettings.masterTranslationFile.filename
        );
        try {
            const fileData = fs.readFileSync(filePath, 'utf8');
            const translations = JSON.parse(fileData);

            return searchTranslationValues(translations, searchValue);
        } catch (error) {
            console.error('Error reading or parsing file:', error);
            return [];
        }
    },

    searchTranslationKey: (searchValue) => {
        i18nFoldersPaths = paths.getFilePaths();
        globalSettings = getGlobalSettings();
        const filePath = path.join(
            getFilePaths()[globalSettings.masterTranslationFile.i18nFolderIndex],
            globalSettings.masterTranslationFile.filename
        );
        try {
            const fileData = fs.readFileSync(filePath, 'utf8');
            const translations = JSON.parse(fileData);

            return searchTranslationKeys(translations, searchValue);
        } catch (error) {
            console.error('Error reading or parsing file:', error);
            return [];
        }
    },

    compareFiles: () => {
        i18nFoldersPaths = paths.getFilePaths();
        globalSettings = getGlobalSettings();
        let startTime;
        let endTime;
        startTime = performance.now();

        const filePath = path.join(
            getFilePaths()[globalSettings.masterTranslationFile.i18nFolderIndex],
            globalSettings.masterTranslationFile.filename
        );
        const masterData = JSON.parse(fs.readFileSync(filePath, 'utf8'));
        const diffInFiles = [];

        i18nFoldersPaths.forEach((i18nFolderPath, i18nPathIndex) => {
            const files = fs.readdirSync(i18nFolderPath);

            files.forEach((file, fileIndex) => {
                const fileDiff = [];
                const filePath = path.join(i18nFolderPath, file);
                const fileData = JSON.parse(fs.readFileSync(filePath, 'utf8'));

                const removeLanguageFlag = (value) => {
                    if (!value || typeof value === 'object') {
                        return value;
                    }
                    return value.replace(/-\w+$/, '');
                };

                const compareKeys = (masterObj, ComparedObj, masterCompare = true, currentPath = '') => {
                    Object.keys(masterObj).forEach((key) => {
                        if (Object.prototype.hasOwnProperty.call(masterObj, key)) {
                            const fullPath = currentPath ? `${currentPath}.${key}` : key;

                            if (typeof masterObj[key] === 'object' && typeof ComparedObj[key] === 'object') {
                                compareKeys(masterObj[key], ComparedObj[key], masterCompare, fullPath);
                            } else {
                                const cleanMasterValue = removeLanguageFlag(masterObj[key]);
                                const cleanCurrentValue = removeLanguageFlag(ComparedObj[key]);
                                if (!masterCompare && cleanMasterValue && cleanCurrentValue) {
                                    // skipping - already included in diff with master file
                                } else {
                                    if (cleanMasterValue !== cleanCurrentValue) {
                                        fileDiff.push({
                                            id: fileDiff.length + 1,
                                            translationKey: fullPath,
                                            // allows to show diff on both sides
                                            comparisonValue: masterCompare ? masterObj[key] : ComparedObj[key],
                                            currentValue: masterCompare ? ComparedObj[key] : masterObj[key]
                                        });
                                    }
                                }
                            }
                        }
                    });
                };

                compareKeys(masterData, fileData);
                compareKeys(fileData, masterData, false);

                if (fileDiff.length) {
                    diffInFiles.push({
                        id: i18nPathIndex + fileIndex + 1,
                        filename: file,
                        path: i18nFolderPath,
                        diff: fileDiff
                    });
                }
            });
        });

        endTime = performance.now();
        let elapsedTime = (endTime - startTime);
        if (elapsedTime < 0.00001) {
            elapsedTime = 0.00;
        } else {
            elapsedTime = +elapsedTime.toFixed(4); // Convert to 4 decimal places
        }
        console.log(`Iteration took ${elapsedTime} milliseconds.`);

        return { globalSettings, i18nFoldersPaths, diff: diffInFiles };
    }
}

const searchTranslationValues = (translations, searchValue) => {
    const results = [];
    let startTime;
    let endTime;

    function searchInObject(obj, path = []) {
        startTime = performance.now();
        for (let key in obj) {
            if (typeof obj[key] === 'object' && !Array.isArray(obj[key])) {
                searchInObject(obj[key], path.concat(key));
            } else if (typeof obj[key] === 'string' && obj[key].toLowerCase().includes(searchValue.toLowerCase())) {
                results.push({
                    id: results.length + 1,
                    path: path.concat(key).join('.'),
                    value: obj[key]
                });
            }
        }
        endTime = performance.now();
    }

    searchInObject(translations);

    let elapsedTime = (endTime - startTime);
    if (elapsedTime < 0.00001) {
        elapsedTime = 0.00;
    } else {
        elapsedTime = +elapsedTime.toFixed(4); // Convert to 4 decimal places
    }
    console.log(`Iteration took ${elapsedTime} milliseconds.`);

    return results;
}

const searchTranslationKeys = (translations, searchValue) => {
    let results = [];
    let startTime;
    let endTime;

    function searchInObject(obj, path = []) {
        startTime = performance.now();
        if (searchValue.includes('.')) {
            results = searchTranslation(translations, searchValue);
        } else {
            for (let key in obj) {
                if (key.toLowerCase().includes(searchValue.toLowerCase())) {
                    results.push({
                        id: results.length + 1,
                        path: path.concat(key).join('.'),
                        key: key,
                        value: obj[key]
                    });
                }
                if (typeof obj[key] === 'object' && !Array.isArray(obj[key])) {
                    searchInObject(obj[key], path.concat(key));
                }
            }
        }
        endTime = performance.now();
    }


    searchInObject(translations);

    let elapsedTime = (endTime - startTime);
    if (elapsedTime < 0.00001) {
        elapsedTime = 0.00;
    } else {
        elapsedTime = +elapsedTime.toFixed(4); // Convert to 4 decimal places
    }
    console.log(`Iteration took ${elapsedTime} milliseconds.`);

    return results;
}
