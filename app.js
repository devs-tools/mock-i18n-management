const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const port = 3000;

const apiController = require('./backend/api-controller');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Serve static files
app.use('/frontend/assets', express.static(path.join(__dirname, 'frontend/assets')));
app.use('/views', express.static(path.join(__dirname, 'frontend/views')));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/frontend/views'));

// APP
app.get('/', (req, res) => {
    res.render('index', { warning: null, existingKey: null, existingValue: null });
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});

// Mounting the API routes
app.use('/api', apiController);
