let i18nFoldersPaths;
let relativeToPath;
let htmlFileDisplay;
let currentPathsContainer;
let relativeToPathsContainer;
let newPathsInputContainer;
let updateFolderPathResultsDisplayContainer;


function initFilepaths() {
    currentPathsContainer = document.getElementById('currentPathsContainer');
    relativeToPathsContainer = document.getElementById('relativeToPathsContainer');
    newPathsInputContainer = document.getElementById('newPathInputContainer');
    updateFolderPathResultsDisplayContainer = document.getElementById('resultsDisplay');
    getFilesPaths();
}


function getFilesPaths() {
    fetch('/api/getFilePaths') // Route to retrieve file paths from the backend
        .then(response => response.json())
        .then(filePaths => {
            i18nFoldersPaths = filePaths.i18nFoldersPaths;
            relativeToPath = filePaths.relativeTo;

            setupCurrentPathsContainer();
            setupNewPathsInputContainer();
        })
        .catch(error => {
            console.error('Error fetching paths:', error);
        });
}

function setupCurrentPathsContainer() {
    htmlFileDisplay = '';
    if (i18nFoldersPaths.length) {
        i18nFoldersPaths.forEach(singlePath => {
            htmlFileDisplay += `<div class="base-border search-result">
                                   <span class="ml-2">${singlePath}</span>
                                </div>`
        });
    } else {
        htmlFileDisplay += `<div class="base-border search-result">
                               <span class="ml-2 warning">NONE</span>
                            </div>`
    }
    currentPathsContainer.innerHTML = htmlFileDisplay;

    relativeToPathsContainer.innerHTML = `<div class="base-border search-result">
                                              <span class="ml-2">${relativeToPath}</span>
                                          </div>`;
}

function setupNewPathsInputContainer() {
    newPathsInputContainer.innerHTML = '';
    if (i18nFoldersPaths.length) {
        i18nFoldersPaths.forEach((singlePath, i) => {
            newPathsInputContainer.innerHTML += `<div class="mt-1 ml-1">
                                                    <input 
                                                        class="pr-1 w-100 text-right" 
                                                        type="text" 
                                                        value="${singlePath}"
                                                        id="folderPath${i}" 
                                                        name="folderPath${i}" />
                                                </div>`
        });
    } else {
        newPathsInputContainer.innerHTML += `<div class="mt-1 ml-1">
                                                 <input 
                                                     class="pr-1 w-100 text-right" 
                                                     type="text" 
                                                     id="folderPath1" 
                                                     name="folderPath1" />
                                             </div>`
    }
}

function addPathInput() {
    const pathsLength = newPathsInputContainer.children.length;
    const newSubContainer = document.createElement('div');

    newSubContainer.className = 'mt-1 ml-1';
    newSubContainer.innerHTML = `<input class="pr-1 w-100 text-right"
                                        type="text"
                                        id="folderPath${pathsLength + 1}"
                                        name="folderPath${pathsLength + 1}">`;

    newPathsInputContainer.appendChild(newSubContainer)
}

function removePathInput() {
    const pathsLength = newPathsInputContainer.children.length;
    if (pathsLength > 1) {
        newPathsInputContainer.removeChild(newPathsInputContainer.children[pathsLength - 1])
    }
}

function updateFilepaths() {
    const folderPaths = [];
    for (let i = 0; i < newPathsInputContainer.children.length; i++) {
        folderPaths.push(newPathsInputContainer.children[i].firstChild.value)
    }

    fetch('/api/updatePaths', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({folderPaths}),
    })
        .then(response => {
            // Handle response from server if needed
            console.log('update file path successful', response)
            updateFolderPathResultsDisplayContainer.innerHTML = `i18n folder path updated successfully`;
        })
        .catch(error => {
            // Handle error if the request fails
            console.log('ERROR update file path successful', error)
            updateFolderPathResultsDisplayContainer.innerHTML = `An error occurred while updating the i18n folder path.`;
        });
}
