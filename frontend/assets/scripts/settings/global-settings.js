let globalSettings;
let i18nPathsSettings;

let uppercaseInputContainer;
let suffixInMasterLanguageContainer;
let folderSelectContainer;
let fileSelectContainer;

function initGlobalSettings() {
    uppercaseInputContainer = document.getElementById('uppercaseInputContainer')
    suffixInMasterLanguageContainer = document.getElementById('suffixInMasterLanguageContainer')
    folderSelectContainer = document.getElementById('folderSelectContainer')
    fileSelectContainer = document.getElementById('fileSelectContainer')

    loadGlobalSettings();
}

function loadGlobalSettings() {
    fetch('/api/getGlobalSettings')
        .then(response => response.json())
        .then(settings => {
            fetch('/api/getFilePathsSettings') // Route to retrieve file paths from the backend
                .then(response => response.json())
                .then(filePathsSettings => {
                    i18nPathsSettings = filePathsSettings.response;
                    updateSettingsView();
                })
                .catch(error => {
                    console.error('Error fetching paths:', error);
                });
            globalSettings = settings;
        })
        .catch(error => {
            console.error('Error fetching global settings:', error);
        });
}

function updateGlobalSettings() {
    const settings = globalSettings;

    settings.uppercaseLanguageFlag = uppercaseInputContainer.checked;
    settings.suffixInMasterLanguage = suffixInMasterLanguageContainer.checked;
    settings.masterTranslationFile.i18nFolderIndex = +folderSelectContainer.value;
    settings.masterTranslationFile.filename = fileSelectContainer.children[+fileSelectContainer.value + 1].text;

    fetch('/api/updateGlobalSettings', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(settings),
    })
        .then((response) => response.json())
        .then((response) => {
            console.log('Overwrite ok ?', response.settings)
            globalSettings = response.settings;
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

function updateSettingsView() {
    uppercaseInputContainer.checked = globalSettings.uppercaseLanguageFlag;
    suffixInMasterLanguageContainer.checked = globalSettings.suffixInMasterLanguage;
    initFoldersSelectContainer();
    initFilesSelectContainer();

}

function initFoldersSelectContainer() {
    for (let i = 0; i < i18nPathsSettings.length; i++) {
        const option = document.createElement('option')
        option.value = i.toString()
        option.text = i18nPathsSettings[i].folderPath;
        folderSelectContainer.appendChild(option)
    }
    folderSelectContainer.children[globalSettings.masterTranslationFile.i18nFolderIndex].selected = true
}

function initFilesSelectContainer() {
    const selectedMasterFileIndex = globalSettings.masterTranslationFile.i18nFolderIndex;

    populateFilesSelectContainer(selectedMasterFileIndex);
    updateSelectedFile();

}

function updateFilesSelectContainer() {
    const selectedMasterFolderIndex = folderSelectContainer.value;
    const displayDefaultOption = selectedMasterFolderIndex !== globalSettings.masterTranslationFile.i18nFolderIndex.toString();
    populateFilesSelectContainer(selectedMasterFolderIndex, displayDefaultOption);
}

function populateFilesSelectContainer(folderIndex, withDefault = false) {
    fileSelectContainer.innerHTML = '';
    const option = document.createElement('option')
    option.text = '-- Select master file --'
    fileSelectContainer.prepend(option)
    for (let i = 0; i < i18nPathsSettings[folderIndex].files.length; i++) {
        const option = document.createElement('option')
        option.value = i.toString()
        option.text = i18nPathsSettings[folderIndex].files[i];
        fileSelectContainer.appendChild(option)
    }
    if (!withDefault) {
        updateSelectedFile()
    }
}

function updateSelectedFile() {
    for (let i = 0; i < fileSelectContainer.children.length; i++) {
        if (fileSelectContainer[i].text === globalSettings.masterTranslationFile.filename) {
            fileSelectContainer[i].selected = true
        }
    }
}