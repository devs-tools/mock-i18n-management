function saveTranslationKeyValue() {
    const key = document.getElementById('key').value;
    const value = document.getElementById('value').value;
    if (key && value) {
        const data = {key: key, value: value};

        fetch('/api/update', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
            .then((response) => response.json())
            .then((data) => {
                const warning = data.warning;
                const existingData = data.existingData;
                const filesWhereNotPresent = data?.filesWhereNotPresent;
                if (warning) {
                    if (existingData) {
                        let html = '<h4 class="warning">Existing translation(s):</h4>';
                        existingData.forEach((data, index) => {
                            html += `<div class="base-border flex-row search-result">
                                <span class="col-1">${index + 1}</span>
                                <span class="col-11">
                                    <div class="flex-row">
                                        <span class="col-6">"${data.file}":</span>
                                        <span class="col-6">${formatValue(data.existingValue)}</span>
                                    </div>
                                </span>
                            </div>`;
                        });
                        document.querySelector('.existingData').innerHTML = html;
                    }
                    if (filesWhereNotPresent.length) {
                        let html = '<h4 class="warning">File(s) where the translation is not present:</h4>';
                        filesWhereNotPresent.forEach((data, index) => {
                            html += `<div class="base-border flex-row search-result">
                                <span class="col-1">${index + 1}</span>
                                <span class="col-11">
                                    <div class="flex-row">
                                        <span class="col-12">"${data}"</span>
                                    </div>
                                </span>
                            </div>`;
                        });
                        document.querySelector('.notInEachFile').innerHTML = html;
                    }

                    // Show the warning and confirm button
                    document.querySelector('.warning').innerHTML = warning;
                    document.getElementById('existingTranslation').style.display = 'block';
                    document.getElementById('submitButton').style.display = 'none';
                } else {
                    // Update successful, reset the form and hide the confirm button
                    document.getElementById('updateForm').reset();
                    document.getElementById('existingTranslation').style.display = 'none';
                    document.getElementById('submitButton').style.display = 'block';
                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }
    else {
        formFocusOut('key');
        formFocusOut('value');
    }
}

function overwriteTranslationValue() {
    const key = document.getElementById('key').value;
    const value = document.getElementById('value').value;

    const data = {key: key, value: value, confirmation: true};

    fetch('/api/update', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => {
            console.log('Overwrite ok ?', data)
            resetContent(true);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

let keyFormErrorContainer;
let valueFormErrorContainer;

let keyInputContainer;
let valueInputContainer;

function formSetup() {
    const updateForm = document.getElementById('updateForm');

    if (updateForm) {
        keyFormErrorContainer = document.getElementById('keyFormError');
        valueFormErrorContainer = document.getElementById('valueFormError');

        keyInputContainer = document.getElementById('key');
        valueInputContainer = document.getElementById('value');
    }
}

function formFocusIn(inputId) {
    if (inputId === 'key' && keyFormErrorContainer.innerHTML) {
        keyFormErrorContainer.innerHTML = '';
    } else if (inputId === 'value' && valueFormErrorContainer.innerHTML) {
        valueFormErrorContainer.innerHTML = ''
    }
}

function formFocusOut(inputId) {
    if (inputId === 'key' && !keyInputContainer.value) {
        keyFormErrorContainer.innerHTML = '<div><span>Missing KEY</span></div>';
    } else if (inputId === 'value' && !valueInputContainer.value) {
        valueFormErrorContainer.innerHTML = '<div><span>Missing VALUE</span></div>'
    }
}

function formatValue(value) {
    if (typeof value === 'object') {
        return `<pre class="m-0">${JSON.stringify(replaceInObject(value), null, 4)}</pre>`;
    } else {
        return `"${value.toString().replaceAll('<', '&lt;')}"`;
    }
}

function replaceInObject(obj) {
    for (let key in obj) {
        if (typeof obj[key] === 'string') {
            obj[key] = obj[key].replaceAll('<', '&lt;');
        } else if (typeof obj[key] === 'object' && obj[key] !== null) {
            obj[key] = replaceInObject(obj[key]);
        }
    }
    return obj;
}

function resetContent(resetForm) {
    document.querySelector('.existingData').innerHTML = '';
    document.querySelector('.notInEachFile').innerHTML = '';

    // Update successful, reset the form and hide the confirm button
    if (resetForm) {
        document.getElementById('updateForm').reset();
    }
    document.querySelector('.warning').innerHTML = '';
    document.getElementById('existingTranslation').style.display = 'none';
    document.getElementById('submitButton').style.display = 'block';
}