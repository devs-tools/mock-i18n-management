let masterFilePathContainer;
let diffsResultsContainer;
let compareResultsDisplayContainer;
// let actionContainer;

function initCompareFiles() {
    masterFilePathContainer = document.getElementById('masterFilePathContainer');
    diffsResultsContainer = document.getElementById('diffsResultsContainer');
    compareResultsDisplayContainer = document.getElementById('resultsDisplay');
    // actionContainer = document.getElementById('actionContainer');

    loadCompareFiles();
}

function loadCompareFiles() {
    fetch('/api/compare') // Route to retrieve file paths from the backend
        .then(res => res.json())
        .then(data => {
            globalSettings = data.globalSettings;
            i18nFoldersPaths = data.i18nFoldersPaths;
            let html = '';
            let diffCount = 0;
            data.diff.forEach((diffInFile) => {
                html += `<h3 class="pl-1 base-border flex-row search-result">
                            <span class="col-1">${diffInFile.id}</span>
                            <span class="col-4">${diffInFile.path}/${diffInFile.filename}</span>
                            <span class="col-4"></span>
                            <span class="col-3"></span>
                        </h3>`;

                diffCount += diffInFile.diff.length;

                diffInFile.diff.forEach(element => {
                    const masterValue = formatValue(element.comparisonValue);
                    const currentValue = formatValue(element.currentValue);

                    html += `<div class="base-border search-result">
                                 <div class="flex-row mt-1 fw-bold">
                                     <span class="col-1"></span>
                                     <span class="col-1">${diffInFile.id}.${element.id}</span>
                                     <span class="col-4 flex-end translation-key-label">Translation key: </span>
                                     <span class="col-4 translation-key-value">"${element.translationKey}"</span>
                                 </div>
                                 <div class="flex-between mx-1 mt-1">
                                     <div class="comparison-value ${!masterValue ? 'missing-value' : 'master-value'}">
                                         ${masterValue}
                                     </div>
                                     <div class="comparison-value missing-value">
                                         ${currentValue}
                                     </div>
                                 </div>
                             </div>`;
                });
                html += `<br />`;
            });
            diffsResultsContainer.innerHTML = html;

            compareResultsDisplayContainer.innerHTML = `<span class="search-results-count">
                                                           ${diffCount} Difference(s) found in ${data.diff.length} file(s)
                                                       </span>`;

            masterFilePathContainer.innerHTML =
                `<h3 class="m-0">
                    Master file: ${i18nFoldersPaths[globalSettings.masterTranslationFile.i18nFolderIndex]}
                    /${globalSettings.masterTranslationFile.filename}
                </h3>`;
        })
        .catch(error => {
            console.error('Error fetching paths:', error);
        });

    // todo fix files
    // actionContainer.innerHTML = `<button class="btn btn--small" onclick="fixAll()">Fix all files</button>`;
}

function fixAll() {
    console.log('fix all action')
}

function formatValue(value) {
    if (!value) {
        return ''
    }
    if (typeof value === 'object') {
        return `<pre class="m-0">${JSON.stringify(replaceInObject(value), null, 4)}</pre>`;
    } else {
        return `"${value.toString().replaceAll('<', '&lt;')}"`;
    }
}

function replaceInObject(obj) {
    for (let key in obj) {
        if (typeof obj[key] === 'string') {
            obj[key] = obj[key].replaceAll('<', '&lt;');
        } else if (typeof obj[key] === 'object' && obj[key] !== null) {
            obj[key] = replaceInObject(obj[key]);
        }
    }
    return obj;
}