let searchKeyInputContainer;
let searchKeyResultsContainer;
let searchKeyResultsDisplayContainer;

function initSearchKey() {
    searchKeyInputContainer = document.getElementById('searchKeyInput');
    searchKeyResultsContainer = document.getElementById('searchKeyResultsContainer');
    searchKeyResultsDisplayContainer = document.getElementById('resultsDisplay');
}

function searchKey() {
    const searchQuery = searchKeyInputContainer.value;
    if (searchQuery) {
        fetch(`/api/search-key/?term=${searchQuery}`)
            .then(response => response.json())
            .then(searchResults => {
                let html = '';
                for (let result of searchResults) {
                    html += `<div class="base-border flex-row search-result">
                                <span class="col-1">${result.id}</span>
                                <span class="col-11">
                                    <div class="flex-row">
                                        <span class="col-6">"${result.path}":</span>
                                        <span class="col-6">${formatValue(result.value)}</span>
                                    </div>
                                </span>
                            </div>`;
                }
                searchKeyResultsContainer.innerHTML = html;

                searchKeyResultsDisplayContainer.innerHTML = `<span class="search-results-count">
                                                         Results: ${searchResults.length}
                                                     </span>`;
            })
            .catch(error => {
                searchKeyResultsContainer.innerHTML =
                    `<div class="base-border mt-1">
                        <span class="mx-1">An error occurred</span>
                        <span class="mx-1">${error}</span>
                    </div>`;
                console.error('Error fetching paths:', error);
            });
    }
}

function formatValue(value) {
    if (Array.isArray(value)) {
        let formatted = '';
        value.forEach(element => {
            formatted+= `<pre class="m-0">${JSON.stringify(replaceInObject(element), null, 4)}</pre>`
        })
        return formatted;
    } else if (typeof value === 'object') {
        return `<pre class="m-0">${JSON.stringify(replaceInObject(value), null, 4)}</pre>`;
    } else {
        return `"${value.toString().replaceAll('<', '&lt;')}"`;
    }
}

function replaceInObject(obj) {
    for (let key in obj) {
        if (typeof obj[key] === 'string') {
            obj[key] = obj[key].replaceAll('<', '&lt;');
        } else if (typeof obj[key] === 'object' && obj[key] !== null) {
            obj[key] = replaceInObject(obj[key]);
        }
    }
    return obj;
}