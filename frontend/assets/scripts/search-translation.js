let searchInputContainer;
let searchResultsContainer;
let searchResultsDisplayContainer;

function initSearchTranslation() {
    searchInputContainer = document.getElementById('searchInput');
    searchResultsContainer = document.getElementById('searchResultsContainer');
    searchResultsDisplayContainer = document.getElementById('resultsDisplay');
}

function search() {
    const searchQuery = searchInputContainer.value;
    if (searchQuery) {
        fetch(`/api/search/?term=${searchQuery}`)
            .then(response => response.json())
            .then(searchResults => {
                let html = '';

                for (let result of searchResults) {
                    html += `<div class="base-border flex-row search-result">
                                <span class="col-1">${result.id}</span>
                                <span class="col-11">
                                    <div class="flex-row">
                                        <span class="col-6">"${result.path}":</span>
                                        <span class="col-6">"${result.value.replaceAll('<', '&lt;')}"</span>
                                    </div>
                                </span>
                            </div>`;
                }
                searchResultsContainer.innerHTML = html;

                searchResultsDisplayContainer.innerHTML = `<span class="search-results-count">
                                                         Results: ${searchResults.length}
                                                     </span>`;
            })
            .catch(error => {
                searchResultsContainer.innerHTML =
                    `<div class="base-border mt-1">
                        <span class="mx-1">An error occurred</span>
                        <span class="mx-1">${error}</span>
                    </div>`;
                console.error('Error fetching paths:', error);
            });
    }
}
