let contentContainer;
let globalWarningContainer;
let actionContainer;
let resultsDisplayContainer;
let actionDialogContainer;
let actionDialogTitle;
let actionDialogContent;
let confirmActionButton;

let resultDialogContainer;
let resultDialogTitle;
let resultDialogContent;

let indexGlobalSettings;
let warnings;

function init() {
    contentContainer = document.getElementById('contentContainer');
    globalWarningContainer = document.getElementById('globalWarningContainer');
    actionContainer = document.getElementById('actionContainer');
    resultsDisplayContainer = document.getElementById('resultsDisplay');
    actionDialogContainer = document.getElementById('action-dialog')
    actionDialogTitle = document.getElementById('action-dialog-title')
    actionDialogContent = document.getElementById('action-dialog-content')
    confirmActionButton = document.getElementById('confirm-action')
    resultDialogContainer = document.getElementById('result-dialog')
    resultDialogTitle = document.getElementById('result-dialog-title')
    resultDialogContent = document.getElementById('result-dialog-content')

    loadContent('create-update-translation');
}
function loadContent(fileName) {

    resetContainers();

    fetch(`../views/${fileName}.ejs`)
        .then(response => response.text())
        .then(data => {
            contentContainer.innerHTML = data;
        })
        .catch(error => {
            console.error('Error:', error);
        });
    setTimeout(() => {
        checkWarnings();
        switch (fileName){
            case 'create-update-translation':
                formSetup();
                return;
            case 'search-translation':
                initSearchTranslation();
                return;
            case 'search-key':
                initSearchKey();
                return;
            case 'compare-files':
                initCompareFiles();
                return;

            /* SETTINGS */
            case 'settings/global-settings':
                initGlobalSettings();
                return;
            case 'settings/i18n-folder-paths':
                initFilepaths();
                return;
        }

    }, 500)
}

function resetContainers() {
    contentContainer.innerHTML = '';
    globalWarningContainer.innerHTML = '';
    actionContainer.innerHTML = '';
    resultsDisplayContainer.innerHTML = '';
    actionDialogTitle.innerHTML = '';
    actionDialogContent.innerHTML = '';
}

function checkWarnings() {
    console.log('check warnings FE')
    fetch('/api/getWarnings')
        .then(res => res.json())
        .then((data) => {
            console.log('warnings', data)
            warnings = data.warnings;
            indexGlobalSettings = data.globalSettings;
            let html = '';

            /* add new warnings check here */

            /* Empty folder paths */
            html += setupPathWarning(warnings);

            /* Case suffix files check */
            html += setupCaseWarning(warnings, indexGlobalSettings);

            /* Suffix in master language check */
            html += setupSuffixMasterLanguageWarning(warnings, indexGlobalSettings);


            globalWarningContainer.innerHTML = html;
        });
}

function setupPathWarning(warnings) {
    if (warnings.folderPathsWarnings) {
        return `<div class="pl-1 pb-1 base-border search-result w-50">
                    <h3 class="warning fw-bold"> /!\\ Missing i18n folder path /!\\ </h3>
                    <div class="flex-center">
                        <span class="warning fw-bold">The i18n files folder path is currently undefined.</span>
                    </div>
                
                    <div class="flex-end mt-1">
                        <button class="btn btn--small" onclick="loadContent('settings/i18n-folder-paths')">Set folder(s)</button>
                    </div>
                </div>`;
    }
    return '';
}

function setupCaseWarning(warnings, settings) {
    let caseWarningHtml = '';

    if (warnings.caseWarnings.length) {
        caseWarningHtml += `<div class="pl-1 pb-1 base-border search-result w-75">
                             <h3 class="warning fw-bold"> /!\\ Suffix case setting mismatch /!\\ </h3>
                             <div class="">
                                 <div class="flex-center warning fw-bold mb-1">
                                     <span>The case option is currently set to: </span> 
                                     <span class="text fw-bold">${ settings.uppercaseLanguageFlag ? 'Uppercase' : 'Lowercase' }</span>
                                 </div>
                                 <div class="flex-center warning fw-bold">${ warnings.caseWarnings.length } translation file(s) contain discrepancies.</div>
                             </div>`;

        warnings.caseWarnings.forEach((caseWarning, index) => {
            caseWarningHtml += `<div class="px-1 mt-1 fw-bold">
                                    <div class="flex-row">
                                        <span class="col-1">${index + 1}</span>
                                        <span class="col-6">${ caseWarning.filepath }:</span>
                                        <span class="col-4"></span>
                                        <span class="col-1 flex-end">${ caseWarning.discrepanciesCount }</span>
                                    </div>
                                </div>`;
        });

        caseWarningHtml += `<div class="flex-end mt-1">
                                <button class="btn btn--small" onclick="openConfirmDialog('case')">Fix case warning</button>
                            </div>`

        caseWarningHtml += '</div>';
    }

    return caseWarningHtml;
}

function setupSuffixMasterLanguageWarning(warnings, settings) {
    let suffixMasterLanguageWarningHtml = '';

    if (warnings.suffixMasterFileWarnings.length) {
        suffixMasterLanguageWarningHtml += `<div class="pl-1 pb-1 base-border search-result w-75">
                             <h3 class="warning fw-bold"> /!\\ Suffix in master language file(s) setting mismatch /!\\ </h3>
                             <div class="">
                                 <div class="flex-center warning fw-bold mb-1">
                                     <span>The master language is currently set to: </span> 
                                     <span class="text fw-bold">${ settings.masterTranslationFile.filename.replace('.json', '') }</span>
                                 </div>
                                 <div class="flex-center warning fw-bold">${ warnings.suffixMasterFileWarnings.length } translation file(s) contain discrepancies.</div>
                             </div>`;

        warnings.suffixMasterFileWarnings.forEach((suffixWarning, index) => {
            suffixMasterLanguageWarningHtml += `<div class="px-1 mt-1 fw-bold">
                                                    <div class="flex-row">
                                                        <span class="col-1">${index + 1}</span>
                                                        <span class="col-6">${ suffixWarning.filepath }:</span>
                                                    </div>`;

            if (suffixWarning.discrepanciesCount.extraSuffixCount) {
                suffixMasterLanguageWarningHtml += `<div class="flex-row mt-1">
                                                        <span class="col-2"></span>
                                                        <span class="col-3">Extra suffix: </span>
                                                        <span class="col-7 flex-end">${ suffixWarning.discrepanciesCount.extraSuffixCount }</span>
                                                    </div>`;
            }

            if (suffixWarning.discrepanciesCount.missingSuffixCount) {
                suffixMasterLanguageWarningHtml += `<div class="flex-row mt-1">
                                                        <span class="col-2"></span>
                                                        <span class="col-3">Missing suffix: </span>
                                                        <span class="col-7 flex-end">${ suffixWarning.discrepanciesCount.missingSuffixCount }</span>
                                                    </div>`;
            }
            suffixMasterLanguageWarningHtml += `</div>`;
        });

        suffixMasterLanguageWarningHtml += `<div class="flex-end mt-1">
                                                <button class="btn btn--small" onclick="openConfirmDialog('suffix')">Fix suffix warning</button>
                                            </div>`

        suffixMasterLanguageWarningHtml += '</div>';
    }

    return suffixMasterLanguageWarningHtml;
}

function fixCaseWarnings() {
    fetch('/api/fixCase')
        .then(res => res.json())
        .then(data => {
            console.log('FIXES', data)
            displayFixCaseResults(data.fixCaseResults)
        });
}

function fixSuffixWarnings() {
    console.log('FIX SUFFIX discrepancies')
}

function openConfirmDialog(action) {
    console.log('openConfirmDialog')
    switch (action) {
        case 'case':
            const currentCaseSetting = indexGlobalSettings.uppercaseLanguageFlag ? 'uppercase' : 'lowercase';
            const discrepancy = !indexGlobalSettings.uppercaseLanguageFlag ? 'uppercase' : 'lowercase';
            actionDialogTitle.innerText = 'Confirm auto fix of suffix case warnings';
            actionDialogContent.innerHTML = `<p>You're about to automatically fix the case suffix discrepancies.</p>
                                        <p>This operation will impact ${warnings.caseWarnings.length} files.</p>
                                        <p>All ${discrepancy} suffixes will be converted to ${currentCaseSetting}.</p>`;
            confirmActionButton.onclick = () => confirmAction('case');
            actionDialogContainer.classList.add('show');
            break;
        case 'suffix':
            const extraSuffixesCount = warnings.suffixMasterFileWarnings[0].discrepanciesCount.extraSuffixCount;
            const extraSuffixText = extraSuffixesCount ? `- remove ${extraSuffixesCount} suffixes from the master file.` : ''
            const missingSuffixesCount = warnings.suffixMasterFileWarnings[0].discrepanciesCount.missingSuffixCount;
            const missingSuffixText = missingSuffixesCount ? `- add ${missingSuffixesCount} suffixes to the master file.` : ''
            actionDialogTitle.innerText = 'Confirm auto fix of suffix warnings';
            actionDialogContent.innerHTML = `<p>You're about to automatically fix the suffix discrepancies in master language file.</p>
                                        <p>${extraSuffixText}</p>
                                        <p>${missingSuffixText}</p>`;
            confirmActionButton.onclick = () => confirmAction('suffix');
            openDialog();
            break;
    }

}

function openDialog() {
    actionDialogContainer.classList.add('show');
}
function openResultDialog() {
    resultDialogContainer.classList.add('show');
}
function closeDialog() {
    actionDialogContainer.classList.remove('show');
}
function closeResultDialog() {
    resultDialogContainer.classList.remove('show');
}

function cancelAction() {
    console.log('CANCELED')
    closeDialog();
}

function confirmAction(action) {
    console.log('CONFIRMED', action)
    closeDialog();
    switch (action) {
        case 'case':
            fixCaseWarnings();
            break;
        case 'suffix':
            fixSuffixWarnings();
            break;
        default:
            console.log(`Action: ${action} is not implemented !`)
    }
}

function displayFixCaseResults(data) {
    let html = '';
    data.forEach((file, index) => {
        html += `<div><h3 class="pl-1 base-border flex-row search-result">
                    <span class="col-1">${index + 1}</span>
                    <span class="col-4">${file.filepath}</span>
                    <span class="col-4"></span>
                    <span class="col-3"></span>
                </h3>
                <div class="base-border search-result">`;
        file.changes.forEach((change, changeIndex) => {
            html += `<div class="flex-row mt-1 fw-bold">
                         <span class="col-1 flex-end pr-1">${changeIndex + 1}</span>
                         <div class="col-4 flex-end missing-value mr-1 pr-1">
                             ${change.prev}
                         </div>
                         <div class="col-4 master-value ml-1 pl-1">
                             ${change.curr}
                         </div>
                         <span class="col-1"></span>
                     </div>`
        });
        html += `</div></div>`;
    });
    resultDialogTitle.innerText = 'Fix results: ';
    resultDialogContent.innerHTML = html;
    openResultDialog();
}